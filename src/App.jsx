import React from "react";
import './main.scss';
import { Button } from "./components/Button";
import { Input } from "./components/Input";
import * as math from 'mathjs';

const printNumberKey = [48, 49, 50, 51, 52, 53, 54, 55, 56, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105];
const printOperatorKey = [106, 107, 108, 109, 110, 111];

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			value: '',
			prevValue: '',
			operator: '',
			isEqual: false,
			dote: false,
			end: true
		};
		this.printData = this.printData.bind(this);
		this.handleAnswer = this.handleAnswer.bind(this);
		this.cleanData = this.cleanData.bind(this);
		this.clearLastChar = this.clearLastChar.bind(this);
		this.printDote = this.printDote.bind(this);
		this.printOperator = this.printOperator.bind(this);
		this.handleTurnOn = this.handleTurnOn.bind(this);
		this.handleTurnOff = this.handleTurnOff.bind(this);
		this.handlePressKey = this.handlePressKey.bind(this);
		this.renderPrintOperator = this.renderPrintOperator.bind(this);
	}

	componentDidMount() {
		document.addEventListener('keydown', this.handlePressKey)
	}

	componentWillUnmount() {
		document.removeEventListener('keydown', this.handlePressKey)
	}


	printData(val) {
		this.setState({
			value: this.state.isEqual ? val : this.state.value + val,
			prevValue: this.state.isEqual ? "" : this.state.prevValue,
			isEqual: false,
		})
	}

	printOperator(val) {
		this.setState({
			value: !this.state.operator ? this.state.value + val : this.state.value,
			operator: true,
			dote: false,
			isEqual: false
		})
	}

	handleAnswer() {
		const currentAnswer = !this.state.isEqual ? this.state.value : this.state.prevValue;
		const result = math.eval(this.state.value).toString();

		const roundDigit = (result) => {
			if (result.length > 9 && math.eval(this.state.value) % 1 > 0) {
				return math.eval(this.state.value).toFixed(9)
			}
			if (result.length > 9) {
				return math.eval(this.state.value)
			}
			else {
				return math.eval(this.state.value)
			}
		};


		this.setState({
			value: roundDigit(result),
			isEqual: true,
			operator: false,
			prevValue: currentAnswer
		})
	}

	cleanData() {
		this.setState({
			value: "",
			prevValue: "",
			isEqual: false,
			dote: false,
			operator: false
		})
	}

	clearLastChar() {
		this.setState({
			value: !this.state.isEqual
				? this.state.value.substring(0, this.state.value.length - 1)
				: 0,
			prevValue: !this.state.isEqual ? this.state.isEqual : "",
			operator: false,
			dote: this.state.value.split("").includes('.')
		})
	}

	printDote(val) {
		!this.state.dote
			? this.setState({
				value: this.state.value + val,
				dote: true
			})
			: null;
	}

	handleTurnOn() {
		this.setState({
			end: false,
			isEqual: false
		})
	}

	handleTurnOff() {
		this.setState({
			end: true,
			value: "",
			prevValue: "",
			operator: false
		})
	}

	handlePressKey(event) {
		if(printNumberKey.includes(event.keyCode)){
			this.printData(event.key)
		}
		else if (event.keyCode  === 8) {
			this.clearLastChar(event.key)
		}
		else if(printOperatorKey.includes(event.keyCode)){
			this.printOperator(event.key)
		}
		else if (event.which  === 110){
			this.printDote(".")
		}
		else if (event.keyCode === 13) {
			this.handleAnswer(event.key)
		}
	}

	renderPrintOperator(name, func) {
		return <Button disabled={this.state.end}
					   handleClick={func}
					   name={name}>{name}</Button>
	}

	render() {
		return <div id="block-center">
			<div className="main_block">
				{this.state.end ? <Input output="Turn me on"/> : null}
				{!this.state.end ? <Input output={this.state.prevValue}/> : null}
				{!this.state.end
					? <Input output={this.state.value.length>13 ? "limit" : this.state.value}/>
					: null}
				<div>
					{this.renderPrintOperator("AC", this.cleanData)}
					{this.renderPrintOperator("C", this.clearLastChar)}
					{this.renderPrintOperator("/", this.printOperator)}
					{this.renderPrintOperator("*", this.printOperator)}
				</div>
				<div>
					{this.renderPrintOperator("7", this.printData)}
					{this.renderPrintOperator("8", this.printData)}
					{this.renderPrintOperator("9", this.printData)}
					{this.renderPrintOperator("+", this.printOperator)}
				</div>
				<div>
					{this.renderPrintOperator("4", this.printData)}
					{this.renderPrintOperator("5", this.printData)}
					{this.renderPrintOperator("6", this.printData)}
					{this.renderPrintOperator("-", this.printOperator)}
				</div>
				<div>
					{this.renderPrintOperator("1", this.printData)}
					{this.renderPrintOperator("2", this.printData)}
					{this.renderPrintOperator("3", this.printData)}
					<Button handleClick={this.handleTurnOn} name="On">On</Button>
				</div>
				<div>
					{this.renderPrintOperator("0", this.printData)}
					{this.renderPrintOperator(".", this.printDote)}
					{this.renderPrintOperator("=", this.handleAnswer)}
					<Button handleClick={this.handleTurnOff} name="Off">Off</Button>
				</div>
			</div>
		</div>
	}
}


export default App;