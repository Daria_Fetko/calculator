import React from "react"

const isOperator = val => {
	return !isNaN(val) || val === "." || val === "=";
};

export const Button = props => (
	<button disabled={props.disabled}
		className={`${isOperator(props.name) ? "number" : "operator"}`}
		onClick={() => props.handleClick(props.name)}
		>
		{props.name}</button>
);
