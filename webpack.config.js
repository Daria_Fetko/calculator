const webpack = require("webpack");
const HtmlWebPackPlugin = require("html-webpack-plugin");

module.exports = {
	resolve: {
		extensions: [".js", ".jsx", ".scss"]
	},
	output: {
		publicPath: "./"
	},
	module: {
		rules: [
			{
				test: /\.scss$/,
				use: ['style-loader', 'css-loader', 'sass-loader']
			},
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: [
					{
						loader: "babel-loader",
						options: {
							presets: [
								"env",
								"react",
								"stage-2"
							]
						}
					}
				]
			}
		]
	},
	devServer: {
		port: 9000,
		hot: true,
		inline: true
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.ProvidePlugin({
			React: "react"
		}),
		new HtmlWebPackPlugin({
			template: "./index.html",
			filename: "./index.html"
		})
	]
};